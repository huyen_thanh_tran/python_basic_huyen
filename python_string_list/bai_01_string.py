name = 'hello world'
print(type(name))
print(len(name))
print(name[3])
print(name[0:5])

# string object methods ( string S1)
name = 'xin chao the gioi'
print(name.capitalize())
print(name.upper())
print(name.lower())
print(name.swapcase())
chuoi = " learn python"
print(chuoi.ljust(10, "*"))
print(chuoi.rjust(10, "*"))
print(chuoi.center(14,"*"))
nhap = '****            day la noi dung'
print(len(nhap))
print(nhap.strip("*"))

# string object methods ( string S2)
chuoi = "python la mot ngon ngu lap trinh web"
print('learn' in chuoi)
print(chuoi.index("ngon ngu"))
print(chuoi.replace("mot", "mot trong"))
# split & join strings in python
chuoi ="String là một trong các kiểu phổ biến nhất trong Python. String trong Python là immutable."
print(chuoi.split())
l = chuoi.split()
print(type(l))
print(chuoi.split('b'))
for item in l:
   print(item)
k=""
print(k.join(l))

# method format in string
s = "hello tên tôi là: {} và tuổi của tôi là: {}".format("thanh huyen","23")
print(s)
s= '{:-^20}'.format("EDU.VN")
print(s)