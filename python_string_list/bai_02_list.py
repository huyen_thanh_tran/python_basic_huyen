# list object basic
l = [1,2,3,4,5,6,7,8,9]
print(type(l), id(l), len(l),l[5])
l =l * 2
print(l)
l = l + [l[5] + 1]
print(l)

# treatment methods list
x = [1,2,3,4,5,6,7,8]
print(x.append(9))
print(x.insert(0,9))
z = x.pop(3)
print(z)
y = x.count(5)
print(y)
t = [2,6,2,0,5,7]
print(t.sort())

#queue in list
from collections import deque
queue = deque(["aaa", "bbb", "ccc"])
queue.append("ddd")
queue.append("eee")
queue.popleft()
'aaa'
queue.popleft()
'bbb'
print(queue)

# tối ưu mã nguồn
l = [item ** 2 for item in range(10)]
print(l)
li = [ item for item in l if item % 2==0]
print(li)
la =[1,2,3]
lb = [4,5,6]
lc = [ (x,y) for x in la for y in lb]
print(lc)

