class MyClass:
    def func(self):
        print('Xin chào')
xc = MyClass()
print(MyClass.func)
print(xc.func)
xc.func()

# constructor / destructor in python
class Person:
    def __init__(self, name, age, male):
        print("Class khởi tạo!")
        self.name, self.age, self.male = name, age, male

    def getName(self):
        print("Name:" , (self.name))

    def getAge(self):
        print("Age: " , (self.age))

    def getMale(self):
        print("Male: " , (self.male))

    def __del__(self):
        print('Class  được hủy')
        del self.name, self.age, self.male

person = Person('Trần Văn A', 22, True)
person.getName()
person.getAge()
person.getMale()


# class SoPhuc:
#
#     def __init__(self, r=0, i=0):
#         self.phanthuc = r
#         self.phanao = i
#     def getData(self):
#         print("{}+{}j".format(self.phanthuc, self.phanao))
# c1 = SoPhuc(2, 3)
# c1.getData()
# # Tạo đối tượng số phức mới
# # tạo thêm một thuộc tính mới (new)
# c2 = SoPhuc(5)
# c2.new = 10
# print((c2.phanthuc, c2.phanao, c2.new))

# Inheritance
class Person:
    def __init__(self, name, age):
        self.name, self.age = name, age

    def getName(self):
        print("Name:" , (self.name))

    def getAge(self):
        print("Age:", (self.age))

    def getGt(self):
        print("Gt:", (self.Gt))

    def __del__(self):
        print('Class  được hủy')
        del self.name, self.age

class Male(Person):
    Gt = "Male"
male = Male("Trần Văn A", 22)
male.getName()
male.getAge()
male.getGt()

class CN :
    def __init__(self, name, age):
        self.name, self.age = name, age
    def info(self):
         print("Chào  bạn mính tên là : ", self.name , "mình :", self.age)

    def infograde(self):
        print("Mình học lớp ", self.grade)

class HS(CN) :
 grade = "12/2"
hs = HS("Trần Văn A" , 18 )
hs.info()
hs.infograde()

# File python read write methods
fp = open("data/list.txt","r")
for line in fp:
    print(line, end= "")
fp.close()

fp = open("data/write.txt","w")
fp.write("Hello")
fp.write("\n")
fp.write(" every one")
fp.write("\n")
print("Done")

f = open("data/list.txt","r")
fpos = f.tell()
print("Đọc tới vị trí", fpos)
line = f.readline()
print(line, end ="")
fpos = f.tell()
print("Đọc từ vị trí ",fpos)
f.seek(1)
print("-" * 5)











