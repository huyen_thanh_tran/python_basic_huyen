# viết vòng lặp từ 1 đén 10 và chia thành 10 dòng ( question 01)
for i in range(1, 101):
    print(i, end="")
    if i % 10 == 0:
        print("")

# viết 1 vòng lắp 0 đến 200 in ra số chẵn  ( question 02)
for i in range(0, 201):
    if i % 2 == 0:
        print(i, end="")
if i % 20 == 0:
    print("")


# xây dựng hàm kiểm tra số nguyên tố  ( question 03)
def NguyenTo(n):
    if n < 2:
        return False
    elif n == 2:
        return True
    else:
        kt = True;
        for i in range(2, n):
            if n % i == 0:
                kt = False;
                break

        if kt == True:
            return True
        else:
            return False
        num = int(input("Nhap so :"))
        result = NguyenTo(num)
        if result == True:
            print(num, " la so nguyen to")
        else:
            print(num, " khong la so nguyen to")


# xây dụng hàm chạy từ 1 đến 1000 và chỉ in ra khi là số nguyên tố ( question 04)
def soNguyenTo(n):
    if n <= 1:
        return False
    else:
        for i in range(2, n):
            if n % i == 0:
                return False
            return True
        dem = 0
        for i in range(1, 1000):
            if soNguyenTo(i):
                dem = dem + 1
                print(i, end="")
                if dem % 10 == 0:
                    print("")


# xây dụng hàm chuyển từ độ c sang độ F và ngược lại
d = int(input("độ F :"))


def convertCtoF(d):
    return (d * 1.8) + 32


print(convertCtoF(d))

t = int(input("độ C:"))


def convertFtoC(t):
    return (t - 32) / 1.8


print(convertFtoC(t))

# xây dụng hàm chuyển từ 1 list km thành m
km = int(input("nhập km:"))


def convertkmtom(km):
    return (km * 1000)


print(km, " km bằng", convertkmtom(km), "m")

# áp dụng tính chất đệ quy để xây dụng hàm tính giai thừa 5!

n = int(input("nhập số:"))


def GiaiThua(n):
    if n < 1:
        print("để có giai thừa thì giá trị phải lớn hơn hoăc bằng 1")
        return
    elif n == 1:
        return 1
    else:
        return n * GiaiThua(n - 1)


print("Kết quá :", GiaiThua(n))
