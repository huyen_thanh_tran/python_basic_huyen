# xây dụng 1 hàm myMap
def myMap(fn, C):
    newC = []
    for x in C:
        newC.append(fn(x))
        return newC
    A = [1, 2, 3, 4, 5]
    print(myMap(lambda y: y + 2, A))


#  xây dụng  danh sách theo yêu cầu
for i in range(1, 11):
    print(i, end="")

for i in range(1, 20):
    print(i, end="")
    if i % 2 != 0:
        print("")

for i in range(1, 50, 5):
    print(i, end="")


# xây dựng hàm đếm
def word(l):
    t = ''
    for i in l:
        if len(i) > len(t):
            t = i
            print(t)


A = ["aa", "bbb", "ccccc"]
print(word(A))

# question 15
# giải phương trình bậc nhất
a = int(input("a="))
b = int(input("b="))


def PTBN(a, b):
    if a == 0:
        print("Phương vô nghiệm")
    elif b == 0:
        print("Phương vô số nghiệm")
    else:
        x = -b / a
        return x


print(PTBN(a, b))
# giải phương trình bậc hai
import math

a = float(input('so a:'))
b = float(input('so b:'))
c = float(input('so c:'))


def pt(a, b, c):
    delta = b * b - 4 * a * c
    if delta > 0:
        x1 = (-b + math.sqrt(delta)) / (2 * a)
        x2 = (-b - math.sqrt(delta)) / (2 * a)
        print("phuong trinh bac 2 co hai nghiem phan biet")
        return x1, x2
    elif delta == 0:
        print("phuong trinh co nghiem kep")
        x = -b / (2 * a)
        return x
    else:
        print("phuong trinh vo nghiem")


print(pt(a, b, c))

# Generator để in ra số chẵn trong khoảng từ 0 đến n
n = int(input("nhập n :"))


def generator(n):
    i = 0
    while i <= n:
        if i % 2 == 0:
            yield i
        i += 1
    values = []
    for i in generator(n):
        values.append(str(i))
    print("Cac so chan trong khoang 0 va n ", values)


# đinh nghĩa class có ít nhất 2 method
l = int(input("chiều dài :"))
w = int(input("chiều rộng :"))


class Hinhchunhat(object):
    def __init__(self, l, w):
        self.dai = l
        self.rong = w

    def area(self):
        return self.dai * self.rong

    def cv(self):
        return (self.dai + self.rong) * 2


ahinhchunhat = Hinhchunhat(l, w)
print(ahinhchunhat.area(), ahinhchunhat.cv())


class Animal:
    def __init__(self):
        print("Animal created")

    def name(self):
        print("Animal")

    def eat(self):
        print("eat")


class Dog(Animal):
    def __init__(self):
        Animal.__init__(self)
        print("Dog created")

    def name(self):
        print("Dog")

    def eat(self):
        print("bone")

    def bark(self):
        print("Woof!")


d = Dog()
d.name()
d.eat()
d.bark()
