x = 9
if x > 8 :
    print(" diem tot")
elif x <= 8 and x >=6 :
       print("binh thuong")
else :
    print("trung binh")
# for_loop
t = (1,2,3,4,5,6,7,8,9)
i =0
for n in t :
    print (i , end = "")
    i = i+1

#contine-break
for  let in 'python' :
    if let == 'h':
        continue
    print(let)
for let in 'python':
    if let == 'h':
     break
    print(let)
A = [1, 3, 5, 9, 7, 2, 6, 8, 10]
tong = 0
for a in A:
    tong = tong + a
print("Tổng :", tong)

#while
count = 0
n = 0
while (count < 8):
      print ('Số thứ', n,' là:', count)
      n = n + 1
      count = count + 1
print ("finish!")
# su dung vong lap while tinh tong cac so
n = int ( input("nhập n: "))
tong = 0
i = 1
while ( i <=n) :
     tong = tong +i
     i =i+1
     print("Tổng :" , tong)
  # while vs else
n = 0
while (n < 2) :
      print(n,"nhỏ hơn 2")
      n = n+1
else :
    print(n,"không nhỏ hơn 2")

   #range in python
a = ['Mary', 'had', 'a', 'little', 'lamb']
for i in range(len(a)) :
     print (i, a[i])