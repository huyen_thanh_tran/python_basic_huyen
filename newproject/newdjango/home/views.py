from django.template.response import TemplateResponse
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render
from .forms import RegistrationForm


# Create your views here.
def homepage(request):
    return TemplateResponse(request, 'base.html')


def fashion(request):
    return TemplateResponse(request, 'fashion.html')


def single(request):
    return TemplateResponse(request, 'single.html')


def sports(request):
    return TemplateResponse(request, 'sports.html')


def register(request):
    form = RegistrationForm()
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
        return HttpResponseRedirect('/')
    else:
        form = RegistrationForm()

    return render(request, 'register.html', {'form': form})


def user_login(request):
    context = {}
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            context["error"] = "Error"
            return render(request, "login.html", context)
    else:
        return render(request, "login.html", context)


def user_logout(request):
    if request.method == "POST":
        logout(request)
        return HttpResponseRedirect(reversed('user_login'))
