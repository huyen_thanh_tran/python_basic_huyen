from django.urls import path
from . import views

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('fashion/', views.fashion, name='fashion'),
    path('single/', views.single, name='single'),
    path('sports/', views.sports, name='sports'),
    path('login/', views.user_login, name ="user_login"),
    path('logout/',views.user_logout, name ='user_logout'),
    path('register/',views.register, name = 'register'),


]