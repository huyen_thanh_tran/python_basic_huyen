from django.test import TestCase
from .models import Post


# Create your tests here.
class BlogTests(TestCase):
    Post.objects.create(
        title='myTitle',
        body='just a Test'
    )

    def test_string(self):
        post = Post(title='My entry title')
        self.assertEqual(str(post), post.title)

    def test_post_list(self):
        reponse = self.client.get('/appnews/')
        self.assertEqual(reponse.status_code, 200)
        self.assertContains(reponse, 'just a Test')
        self.assertTemplateNotUsed(reponse, 'blog.html')

    def test_post_detail(self):
        reponse = self.client.get('/appnews/1/')
        self.assertEqual(reponse.status_code, 200)
        self.assertContains(reponse, 'myTitle')
        self.assertTemplateNotUsed(reponse, 'post.html')
